#include "ThreadManager.h"
#include "uthreads.h"

#define SUCCESS_CODE 0
#define FAILURE_CODE -1
#define SYSTEM_ERROR_MESSAGE "system error: "
#define LIBRARY_ERROR_MESSAGE "thread library error: "
#define SIGACTION_ERROR_MESSAGE "sigaction error."
#define SET_TIMER_ERROR_MESSAGE "setitimer error."
#define USECS_ERROR_MESSAGE "quantum_usecs should be positive."
#define NON_EXISTING_THREAD_ERROR_MESSAGE "You tried to perform an action on a non existing thread."
#define WRONG_THREAD_LOCKING_MUTEX_ERROR_MESSAGE "You cannot unlock a mutex that was locked by another thread."
#define DOUBLE_UNLOCK_ERROR_MESSAGE "You cannot unlock an unlocked mutex."
#define DOUBLE_LOCK_ERROR_MESSAGE "You cannot lock a locked mutex."
#define UNBLOCKABLE_MAIN_THREAD_ERROR_MESSAGE "The main thread is unblockable."
#define REMOVING_NON_EXISTING_ELEMENT_ERROR_MESSAGE "You tried removing tid that was not in the list."
#define EMPTY_READY_THREAD_ERROR_MESSAGE "The main thread should be always in ready_threads."
#define NOT_READY_IN_READY_THREAD_ERROR_MESSAGE "The ready_threads should include only ready threads."
#define BAD_MAP_DELETE_ERROR_MESSAGE "Problem deleting from map."
#define MAX_THREAD_ERROR_MESSAGE "You cannot create more then MAX_THREAD_NUM - 1 threads."
#define SIGPROCMASK_ERROR_MESSAGE "sigprocmask failed."
#define SIGEMPTYSET_ERROR_MESSAGE "sigemptyset failed."
#define SIGFILLSET_ERROR_MESSAGE "sigfillset failed."
#define MAIN_THREAD_ID 0
#define DIRECT_JUMP 0
#define MUTEX_UNLOCKED -1
#define THREAD second
#define ERASE_FAILED 0
#define MICROSECONDS_TO_SECONDS_FACTOR 1000000


using namespace std;
typedef uintptr_t address_t;

#ifdef __x86_64__
/* code for 64 bit Intel arch */

typedef unsigned long address_t;
#define JB_SP 6
#define JB_PC 7

/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr) {
    address_t ret;
    asm volatile("xor    %%fs:0x30,%0\n"
                 "rol    $0x11,%0\n"
    : "=g" (ret)
    : "0" (addr));
    return ret;
}

#else
/* code for 32 bit Intel arch */

typedef unsigned int address_t;
#define JB_SP 4
#define JB_PC 5

/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%gs:0x18,%0\n"
                 "rol    $0x9,%0\n"
    : "=g" (ret)
    : "0" (addr));
    return ret;
}

#endif

char stacks[MAX_THREAD_NUM][STACK_SIZE];
sigjmp_buf environments[MAX_THREAD_NUM];


ThreadManager *ThreadManager::_thread_manager{nullptr};

int ThreadManager::current_running_thread;
int ThreadManager::mutex;

map<int, Thread *> ThreadManager::threads;
list<Thread *> ThreadManager::ready_threads;
list<Thread *> ThreadManager::mutex_blocked_threads;
struct itimerval ThreadManager::timer;
int ThreadManager::quantum_counter;

ThreadManager::ThreadManager(const int quantum_usecs) {
    if (quantum_usecs <= 0) {
        ThreadManager::_print_error(LIBRARY, USECS_ERROR_MESSAGE);
        throw FAILURE_CODE;
    }
    ThreadManager::quantum_counter = 1;
    mutex = MUTEX_UNLOCKED;
    auto *main_thread = new Thread(nullptr, MAIN_THREAD_ID);
    ThreadManager::threads[MAIN_THREAD_ID] = main_thread;
    main_thread->set_state(RUNNING);
    main_thread->increment_quantum_counter();

    struct sigaction sa = {0};

    // Install _timer_handler as the signal handler for SIGVTALRM.
    sa.sa_handler = &_timer_handler;
    if (sigaction(SIGVTALRM, &sa, NULL) == FAILURE_CODE) {
        ThreadManager::_print_error(SYSTEM, SIGACTION_ERROR_MESSAGE);
        delete (main_thread);
        throw FAILURE_CODE;
    }


    // first time interval, seconds part
    ThreadManager::timer.it_value.tv_sec = quantum_usecs / MICROSECONDS_TO_SECONDS_FACTOR;
    // first time interval, microseconds part
    ThreadManager::timer.it_value.tv_usec = quantum_usecs % MICROSECONDS_TO_SECONDS_FACTOR;
    // following time intervals, seconds part
    ThreadManager::timer.it_interval.tv_sec = quantum_usecs / MICROSECONDS_TO_SECONDS_FACTOR;
    // following time intervals, microseconds part
    ThreadManager::timer.it_interval.tv_usec = quantum_usecs % MICROSECONDS_TO_SECONDS_FACTOR;

    // Start a virtual timer. It counts down whenever this process is executing.
    if (setitimer(ITIMER_VIRTUAL, &ThreadManager::timer, NULL)) {
        ThreadManager::_print_error(SYSTEM, SET_TIMER_ERROR_MESSAGE);
        delete (main_thread);
        throw FAILURE_CODE;
    }
}

ThreadManager *ThreadManager::get_thread_manager(const int quantom_usecs) {
    if (_thread_manager == nullptr) {
        try {
            _thread_manager = new ThreadManager(quantom_usecs);
        } catch (int code) {
            _thread_manager = nullptr;
            throw FAILURE_CODE;
        }
    }
    return _thread_manager;
}

int ThreadManager::_setup_stack(int tid) {
    address_t sp, pc;
    sp = (address_t) (stacks[tid]) + STACK_SIZE - sizeof(address_t);
    pc = (address_t) (threads[tid]->get_thread_function());
    sigsetjmp(environments[tid], 1);
    ((environments[tid])->__jmpbuf)[JB_SP] = translate_address(sp);
    ((environments[tid])->__jmpbuf)[JB_PC] = translate_address(pc);
    if (sigemptyset(&(environments[tid]->__saved_mask)) == FAILURE_CODE) {
        ThreadManager::_print_error(SYSTEM, SIGEMPTYSET_ERROR_MESSAGE);
        return FAILURE_CODE;
    }
    return SUCCESS_CODE;
}

int ThreadManager::_change_threads() {
    if (ThreadManager::ready_threads.empty()) {
        ThreadManager::_print_error(LIBRARY, EMPTY_READY_THREAD_ERROR_MESSAGE);
        return FAILURE_CODE; // The main thread should be here
    } else if (ThreadManager::ready_threads.front()->get_state() != READY) {
        ThreadManager::_print_error(LIBRARY, NOT_READY_IN_READY_THREAD_ERROR_MESSAGE);
        return FAILURE_CODE;
    }

    // Switch threads with the next ready one:
    int return_value = sigsetjmp(environments[current_running_thread], 1); // save current environment
    if (return_value != DIRECT_JUMP) { // 0 if direct: https://pubs.opengroup.org/onlinepubs/7908799/xsh/sigsetjmp.html
        return SUCCESS_CODE; // this is entered in the thread that was just jumped to
    }
    if (setitimer(ITIMER_VIRTUAL, &ThreadManager::timer, NULL)) {
        ThreadManager::_print_error(SYSTEM, SET_TIMER_ERROR_MESSAGE);
        return FAILURE_CODE;
    }
    Thread *thread = ready_threads.front();
    ready_threads.pop_front();
    thread->set_state(RUNNING);
    thread->increment_quantum_counter();
    quantum_counter++;
    current_running_thread = thread->get_id();
    siglongjmp(environments[current_running_thread], 1); // jump to next thread
}

int ThreadManager::_remove_from_list(list<Thread *> &list_to_use, int tid) {
    for (auto i = list_to_use.begin(); i != list_to_use.end(); i = next(i)) {
        if ((*i)->get_id() == tid) {
            list_to_use.erase(i);
            return SUCCESS_CODE;
        }
    }
    ThreadManager::_print_error(LIBRARY, REMOVING_NON_EXISTING_ELEMENT_ERROR_MESSAGE);
    return FAILURE_CODE;
}

void ThreadManager::_timer_handler(int sig) {
    threads[current_running_thread]->set_state(READY); // after finishing running go back to waiting
    ThreadManager::ready_threads.push_back(threads[current_running_thread]);
    ThreadManager::_change_threads();
}

void ThreadManager::_print_error(const ErrorType error_type, const string &msg) {
    string basic_error_message;
    switch (error_type) {
        case SYSTEM:
            basic_error_message = SYSTEM_ERROR_MESSAGE;
            break;
        case LIBRARY:
            basic_error_message = LIBRARY_ERROR_MESSAGE;
            break;
    }
    cerr << basic_error_message << msg << endl;

}

int ThreadManager::spawn_thread(void (*f)()) {
    auto unsafe_thread_spawn = [](void (*f)()) {
        if (threads.size() == MAX_THREAD_NUM) {
            ThreadManager::_print_error(LIBRARY, MAX_THREAD_ERROR_MESSAGE);
            return FAILURE_CODE; // maximum number of threads reached, can't spawn more
        }
        int tid = 0;
        for (tid = MAIN_THREAD_ID + 1; tid < MAX_THREAD_NUM; tid++) {
            if (threads.find(tid) == threads.end()) {
                break;
            }
        }
        auto *newThread = new Thread(f, tid);
        threads[tid] = newThread;
        ready_threads.push_back(newThread);
        if (_setup_stack(tid) == FAILURE_CODE) {
            return FAILURE_CODE;
        }
        return tid;
    };

    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    // wrap in control of signals so that nothing interrupts us while managing threads.
    int return_value = unsafe_thread_spawn(f);

    if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
        return_value = FAILURE_CODE;
    }
    return return_value;
}

int ThreadManager::terminate_thread(int tid) {
    auto unsafe_terminate_thread = [](int tid) {
        if (threads.find(tid) == threads.end()) {
            ThreadManager::_print_error(LIBRARY, NON_EXISTING_THREAD_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        if (tid == MAIN_THREAD_ID) {
            ThreadManager::_free_memory();
            exit(SUCCESS_CODE);
        }
        Thread *thread = threads[tid];
        bool should_change_threads = false;
        if (thread->get_state() == RUNNING) {
            should_change_threads = true;
        } else if (thread->get_state() == READY) {
            if (_remove_from_list(ready_threads, tid) == FAILURE_CODE) {
                return FAILURE_CODE;
            }
        } else if (thread->get_is_mutex_blocked()) {
            _remove_from_list(mutex_blocked_threads, tid);
        }
        if (mutex == tid) {
            if (ThreadManager::mutex_unlock() == FAILURE_CODE) {
                return FAILURE_CODE;
            }
        }
        delete (threads[tid]);
        if (threads.erase(tid) == ERASE_FAILED) {
            ThreadManager::_print_error(SYSTEM, BAD_MAP_DELETE_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        if (should_change_threads) {
            if (ThreadManager::_change_threads() == FAILURE_CODE) {
                return FAILURE_CODE;
            }
        }
        return SUCCESS_CODE;
    };


    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    // wrap in control of signals so that nothing interrupts us while managing threads.
    int return_value = unsafe_terminate_thread(tid);

    if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
        return_value = FAILURE_CODE;
    }
    return return_value;
}

int ThreadManager::block_thread(int tid) {
    auto unsafe_block_thread = [](int tid) {
        if (threads.find(tid) == threads.end()) {
            ThreadManager::_print_error(LIBRARY, NON_EXISTING_THREAD_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        if (tid == MAIN_THREAD_ID) {
            ThreadManager::_print_error(LIBRARY, UNBLOCKABLE_MAIN_THREAD_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        if (threads[tid]->get_state() == BLOCKED) {
            return SUCCESS_CODE; // Not really necessary but is more readable
        }
        threads[tid]->set_state(BLOCKED);
        if ((!threads[tid]->get_is_mutex_blocked()) && (tid != current_running_thread)) {
            if (_remove_from_list(ready_threads, tid) == FAILURE_CODE) {
                return FAILURE_CODE;
            }
        }
        if (tid == current_running_thread) { // finish running in the middle of the quantum
            if (ThreadManager::_change_threads() == FAILURE_CODE) {
                return FAILURE_CODE;
            }
        }
        return SUCCESS_CODE;
    };

    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    // wrap in control of signals so that nothing interrupts us while managing threads.
    int return_value = unsafe_block_thread(tid);

    if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
        return_value = FAILURE_CODE;
    }
    return return_value;
}

int ThreadManager::resume_thread(int tid) {
    auto unsafe_resume_thread = [](int tid) {
        if (threads.find(tid) == threads.end()) {
            ThreadManager::_print_error(LIBRARY, NON_EXISTING_THREAD_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        Thread *thread = threads[tid];
        if (thread->get_state() == RUNNING || thread->get_state() == READY) {
            return SUCCESS_CODE; // Not really necessary for READY but is more readable
        }
        if (thread->get_is_mutex_blocked()) {
            thread->set_state(MUTEX_BLOCKED);
        } else {
            thread->set_state(READY);
            ready_threads.push_back(thread);
        }
        return SUCCESS_CODE;
    };

    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    // wrap in control of signals so that nothing interrupts us while managing threads.
    int return_value = unsafe_resume_thread(tid);

    if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
        return_value = FAILURE_CODE;
    }
    return return_value;
}

int ThreadManager::_stop_all_signals(sigset_t *mask, sigset_t *old_mask) {
    int return_value = 0;
    return_value = sigfillset(mask);
    if (return_value == FAILURE_CODE) {
        ThreadManager::_print_error(SYSTEM, SIGFILLSET_ERROR_MESSAGE);
        return FAILURE_CODE;
    }
    return_value |= sigprocmask(SIG_SETMASK, mask, old_mask);
    if (return_value != 0) {
        ThreadManager::_print_error(SYSTEM, SIGPROCMASK_ERROR_MESSAGE);
        _free_memory();
        return FAILURE_CODE;
    }
    return SUCCESS_CODE;
}

int ThreadManager::_resume_signals(sigset_t *old, sigset_t *new_mask) {
    int return_value = sigprocmask(SIG_SETMASK, old, new_mask);
    if (return_value != 0) {
        ThreadManager::_print_error(SYSTEM, SIGPROCMASK_ERROR_MESSAGE);
        _free_memory();
        return FAILURE_CODE;
    }
    return SUCCESS_CODE;
}

int ThreadManager::mutex_lock() {
    start:
    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    if (mutex == MUTEX_UNLOCKED) {
        mutex = current_running_thread;
        if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
            return FAILURE_CODE;
        }
        return SUCCESS_CODE;
    } else if (mutex != current_running_thread) { // We are here because the thread is running
        // -> not in BLOCKED state or in ready_threads
        threads[current_running_thread]->set_state(MUTEX_BLOCKED);
        threads[current_running_thread]->set_is_mutex_blocked(true);

        mutex_blocked_threads.push_back(threads[current_running_thread]); // block itself
        if (ThreadManager::_change_threads() == FAILURE_CODE) { // this thread stopped running mid-quantum
            return FAILURE_CODE;
        }
        if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
            return FAILURE_CODE;
        }
        goto start;
    } else {
        ThreadManager::_print_error(LIBRARY, DOUBLE_LOCK_ERROR_MESSAGE);
        if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
            return FAILURE_CODE;
        }
        return FAILURE_CODE;
    }
}

int ThreadManager::mutex_unlock() {
    auto unsafe_mutex_unlock = []() {
        if (mutex == MUTEX_UNLOCKED) {
            ThreadManager::_print_error(LIBRARY, DOUBLE_UNLOCK_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
        if (mutex == current_running_thread) {
            mutex = MUTEX_UNLOCKED;
            Thread *thread = mutex_blocked_threads.front();
            if (thread != nullptr) {
                thread->set_is_mutex_blocked(false);
                if (thread->get_state() == MUTEX_BLOCKED) {
                    thread->set_state(READY); // else - is blocked by thread
                    ThreadManager::ready_threads.push_back(thread);
                }
                mutex_blocked_threads.pop_front();
            }
            return SUCCESS_CODE;
        } else {
            ThreadManager::_print_error(LIBRARY, WRONG_THREAD_LOCKING_MUTEX_ERROR_MESSAGE);
            return FAILURE_CODE;
        }
    };

    sigset_t mask;
    sigset_t old_mask;

    if (_stop_all_signals(&mask, &old_mask) == FAILURE_CODE) {
        return FAILURE_CODE;
    }

    // wrap in control of signals so that nothing interrupts us while managing threads.
    int return_value = unsafe_mutex_unlock();

    if (_resume_signals(&old_mask, &mask) == FAILURE_CODE) {
        return_value = FAILURE_CODE;
    }
    return return_value;
}

int ThreadManager::get_tid() {
    return current_running_thread; // The calling thread is the running one
}

int ThreadManager::get_total_quantums() {
    return quantum_counter;
}

int ThreadManager::get_thread_quantums(int tid) {
    if (threads.find(tid) == threads.end()) {
        ThreadManager::_print_error(LIBRARY, NON_EXISTING_THREAD_ERROR_MESSAGE);
        return FAILURE_CODE;
    }
    return threads[tid]->get_quantum_counter();
}

ThreadManager::~ThreadManager() {
    if (ThreadManager::_thread_manager == nullptr) {
        return;
    }
    ThreadManager::_free_memory();
}

void ThreadManager::_free_memory() {
    sigset_t mask;
    sigset_t old_mask;

    _stop_all_signals(&mask, &old_mask);

    for (const auto &key_value : threads) {
        delete (key_value.THREAD);
    }

    _resume_signals(&old_mask, &mask);
}