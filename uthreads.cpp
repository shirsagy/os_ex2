#include "uthreads.h"
#include "ThreadManager.h"

#define SUCCESS_CODE 0
#define FAILURE_CODE -1 // for completeness sake


int uthread_init(int quantum_usecs) {
    try {
        ThreadManager::get_thread_manager(quantum_usecs);
        return SUCCESS_CODE;
    } catch (int code) {
        return code;
    }
}

int uthread_spawn(void (*f)()) {
    return ThreadManager::spawn_thread(f);
}

int uthread_terminate(int tid) {
    return ThreadManager::terminate_thread(tid);
}

int uthread_block(int tid) {
    return ThreadManager::block_thread(tid);
}

int uthread_resume(int tid) {
    return ThreadManager::resume_thread(tid);
}

int uthread_mutex_lock() {
    return ThreadManager::mutex_lock();
}

int uthread_mutex_unlock() {
    return ThreadManager::mutex_unlock();
}

int uthread_get_tid() {
    return ThreadManager::get_tid();
}

int uthread_get_total_quantums() {
    return ThreadManager::get_total_quantums();
}

int uthread_get_quantums(int tid) {
    return ThreadManager::get_thread_quantums(tid);
}
