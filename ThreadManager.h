#ifndef THREADS_THREADMANAGER_H
#define THREADS_THREADMANAGER_H

#include "Thread.h"

#include <list>
#include <map>
#include <iostream>
#include <signal.h>
#include <setjmp.h>
#include <sys/time.h>
#include <string>

using namespace std;
/**
 * Enum that describes to the error printer which error type prefix to print, force correct input to it.
 */
enum ErrorType {
    SYSTEM, LIBRARY
};

class ThreadManager {

private:
    static ThreadManager *_thread_manager;
    static int quantum_counter;
    static int current_running_thread;
    static int mutex;

    static map<int, Thread *> threads;
    static list<Thread *> ready_threads;
    static list<Thread *> mutex_blocked_threads;
    static struct itimerval timer;

    /**
     * Description: setup the stack of a new Thread, set correct program counter and stack pointer.
     * @param tid - identification number of the new Thread
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int _setup_stack(int tid);

    /**
     * Description: Switch context to the next Thread, saving the current environment, loading the next
     * and jumping.
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int _change_threads();


    /**
     * Description: the handler for the virtual alarm signal, SIGVTALRM, that the timer sends - responsible
     * for changing threads every quantum.
     * @param sig - requirement for a handler function - unused
     */
    static void _timer_handler(int sig);

    /**
     * Description: print an error to stderr in case of one.
     * @param error_type - enum value for either LIBRARY (internal library error) or SYSTEM (some syscall
     *                     has failed)
     * @param msg - specific message to print, e.g. for locking an already locked mutex
     */
    static void _print_error(const ErrorType error_type, const string &msg);

    /**
     * Description: remove a Thread from one of our lists, for example from ready_threads after it has
     * been locked.
     * @param list_to_use - either ready_threads or mutex_blocked_threads, to remove from (by reference)
     * @param tid - the identification number of the Thread
     * @return SUCCESS_CODE if successful and FAILURE_CODE otherwise - thread not found
     */
    static int _remove_from_list(list<Thread *> &list_to_use, int tid);

    /**
     * Description: frees allocated memory before exiting - deletes all Threads (all other memory is local
     * and not saved in heap).
     */
    static void _free_memory();

    /**
     * Description: use SIGPROCMASK to switch to a blocking mask, not allowing interruptions while working
     * on sensitive stuff like spawning a new Thread.
     * @param mask - pointer the new blocking mask
     * @param old_mask - pointer to save the old mask in
     * @return SUCCESS_CODE if successful, FAILURE_CODE if SIGPROCMASK has failed
     */
    static int _stop_all_signals(sigset_t *mask, sigset_t *old_mask);

    /**
     * Description: use SIGPROCMASK to switch back from a blocking mask after finishing sensitive actions.
     * @param old - pointer the old blocking mask to load
     * @param new_mask - pointer to the blocking mask to save in
     * @return SUCCESS_CODE if successful, FAILURE_CODE if SIGPROCMASK has failed
     */
    static int _resume_signals(sigset_t *old, sigset_t *new_mask);

protected:
    /**
     * Description: constructor for ThreadManager, creates the single instance and saves it as a static
     * member; Including startign the signal timer, the main thread and all members.
     * @param quantom_usecs - the length of a quantum in the system in microseconds
     */
    ThreadManager(const int quantom_usecs);

    /**
     * Destructor for the ThreadManager, destroys allocated data - the Threads.
     */
    ~ThreadManager();

public:
    /**
     * Singletons should not be cloneable.
     */
    ThreadManager(ThreadManager &other) = delete;

    /**
     * Singletons should not be assignable.
     */
    void operator=(const ThreadManager &) = delete;

    /**
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     */
    static ThreadManager *get_thread_manager(const int quantom_usecs);

    /**
     * Description: class-static function that handles the same function for the library.
     * @param f - pointer to the function for the new Thread
     * @return SUCCESS_CODE if successful, FAILURE_CODE if max number of threads reached
     */
    static int spawn_thread(void (*f)());

    /**
     * Description: class-static function that handles the same function for the library.
     * @param tid - the ID of the thread that will be terminated
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     * If a thread terminates itself or the main thread is terminated, the function does not return
     */
    static int terminate_thread(int tid);

    /**
     * Description: class-static function that handles the same function for the library.
     * @param tid - the ID of the thread that will be blocked
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int block_thread(int tid);

    /**
     * Description: class-static function that handles the same function for the library.
     * @param tid - the ID of the thread that will be resumed
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int resume_thread(int tid);

    /**
     * Description: class-static function that handles the same function for the library.
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int mutex_lock();

    /**
     * Description: class-static function that handles the same function for the library.
     * @return SUCCESS_CODE if successful, FAILURE_CODE otherwise
     */
    static int mutex_unlock();

    /**
     * Description: class-static function that handles the same function for the library.
     * @return the ID of the calling thread
     */
    static int get_tid();

    /**
     * Description: class-static function that handles the same function for the library.
     * @return The total number of quantums
     */
    static int get_total_quantums();

    /**
     * Description: class-static function that handles the same function for the library.
     * @return  On success, return the number of quantums of the thread with ID tid
     * On failure, return FAILURE_CODE
     */
    static int get_thread_quantums(int tid);
};


#endif //THREADS_THREADMANAGER_H

