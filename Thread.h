#ifndef TESTERS_THREAD_H
#define TESTERS_THREAD_H

enum State
/**
* Enum that describes to the possible states of a thread.
* RUNNING - currently running.
* READY - waiting fot its turn to run.
* BLOCKED - blocked by a thread (and maybe also by mutex).
* MUTEX_BLOCKED - blocked by mutex.
*/
{
    RUNNING, READY, BLOCKED, MUTEX_BLOCKED // if a thread is both blocked by another thread and by mutex, its state
                                            // will be BLOCKED and the flag is_mutex_blocked will be true.
};

class Thread
{
    State state;
    int quantum_counter;
    int id;
    bool is_mutex_blocked;
    void (*thread_function)();
public:

    /**
	 * Description: This function initializes a Thread object. Saves its function and id, and
	 * initializes its quantum_counter to 0 and state to READY.
	 * @param f - the function to start the Thread on (place in rom)
	 * @param id - the identification number of the Thread, for managing against other Threads.
	 * @return - On success, return a pointer to the new Thread.
	 */
    Thread(void (*f)(), int id);

	/**
	 * Description: getter for the Thread's quantum_counter field - how much time it has run since it
	 * started, i.e. number of quantums that passed.
	 * @return - number of quantums since start of thread.
	 */
    int get_quantum_counter() const;

	/**
	 * Description: update the quantum_counter by incrementing it by 1 - will be called whenever the Thread
	 * resumed.
	 */
    void increment_quantum_counter();

	/**
	 * Description: getter for the Thread's state field - is it READY to run, BLOCKED, MUTEX_BLOCKED,
	 * or already RUNNING.
	 * @return - running state of the Thread, from enum.
	 */
    State get_state() const;

	/**
	 * Description: setter for the Thread's state field, for whenever the manager needs to update it (before
	 * or after running for example).
	 */
    void set_state(State new_state);

	/**
	 * Description: getter for the Thread's identification number, the id field, for managing.
	 * @return - Thread's ID.
	 */
    int get_id() const;

	/**
	 * Description: getter for the Thread's function field - will be used when it is first run by the 
	 * manager.
	 */
    void (*get_thread_function())();

    /**
	 * Description: setter for the Thread's is_mutex_blocked field, for whenever the manager needs to update it (before
	 * when releasing from mutex block for example).
	 */
    void set_is_mutex_blocked(bool mutex_is_blocked);

    /**
	 * Description: getter for the Thread's is_mutex_blocked field - is it true or false.
	 * @return - is_mutex_blocked.
	 */
    bool get_is_mutex_blocked() const;
};

#endif //TESTERS_THREAD_H
