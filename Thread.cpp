//
// Created by t8679566 on 24/05/2021.
//

#include <iostream>
#include "Thread.h"

using namespace std;

Thread::Thread(void (*f)(), int id)
{
    this->quantum_counter = 0;
    this->state = READY;
    this->id = id;
    this->thread_function = f;
    this->is_mutex_blocked = false;
}

int Thread::get_quantum_counter() const
{
    return this->quantum_counter;
}

void Thread::increment_quantum_counter()
{
    this->quantum_counter++;
}

State Thread::get_state() const
{
    return this->state;
}

void Thread::set_state(State new_state)
{
    this->state = new_state;
}

int Thread::get_id() const
{
    return this->id;
}

bool Thread::get_is_mutex_blocked() const {
    return this->is_mutex_blocked;
}

void Thread::set_is_mutex_blocked(bool mutex_is_blocked) {
    this->is_mutex_blocked = mutex_is_blocked;
}

void (*Thread::get_thread_function())()
{
    return this->thread_function;
}
